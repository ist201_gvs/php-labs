<?php
use yii\helpers\Html;
?>
<div class="container">
    <h1><?= Html::encode($country['name']) ?></h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Official Name</th>
            <th scope="col">Image</th>
            <th scope="col">Capital</th>
            <th scope="col">Area</th>
            <th scope="col">Currency</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row"><?= Html::encode($country['official_name']) ?></th>
            <td><?= Html::img('@web/images/countries/png100px/' . strtolower($country['code']) . '.png', ['alt' => $country['name']]) ?></td>
            <td><?= Html::encode($country['capital']) ?></td>
            <td><?= Html::encode($country['area']) ?></td>
            <td><?= Html::encode($country['currency']) ?></td>
        </tr>
        </tbody>
    </table>
</div>