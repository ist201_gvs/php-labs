<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="row">
    <?php foreach ($continents as $continentItem): ?>
        <div class="col-6 col-md-4">
            <div class="card p-4 mb-5">
                <?= Html::img('@web/images/continents/' . strtolower($continentItem['code']) . '.png', ['alt' => $continentItem['name']]) ?>
                <div class="card-body">
                    <h5 class="card-title"><a href='<?= Url::to(['continent/view', 'continent_id' => $continentItem['continent_id']]); ?>'>
                            <?= Html::encode($continentItem['name']) ?></a></h5>
                    <p class="card-text"><?= Html::encode($continentItem['description']) ?></p>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
