<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Country;

class CountryController extends Controller
{
    public function actionView($country_code)
    {
        $country = Country::find()->where(['code' => $country_code])->one();
        return $this->render('index', ['country' => $country]);
    }
}