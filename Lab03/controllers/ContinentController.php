<?php

namespace app\controllers;

use app\models\Continent;
use app\models\Country;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class ContinentController extends Controller
{
    public function actionIndex()
    {
        $continents = Continent::find()->asArray()->all();
        return $this->render('index', ['continents' => $continents]);
    }
    public function actionView($code = "AF", $continent_id = 1)
    {
        $continent = Continent::find()->where(['continent_id' => $continent_id])->one();
        $countriesDataProvider = new ActiveDataProvider([
            'query' => Country::find()->where(['continent_id' => $continent_id]),
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);
        return $this->render('view', compact(['continent', 'countriesDataProvider']));
    }
}
