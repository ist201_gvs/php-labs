<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {packages: ["orgchart"]});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Name');
            data.addColumn('string', 'Manager');
            data.addColumn('string', 'ToolTip');

            data.addRows([

                <?php foreach($departments as $department):?>
                <?php
                switch ($department['type_name']) {
                    case 'Board':
                        $color = 'green';
                        break;
                    case 'Center':
                        $color = 'blue';
                        break;
                    default:
                        $color = 'red';
                }
                ?>
                [{
                    'v': "<?= $department['department_name']?>",
                    'f': "<?= $department['department_name']?> <div style='color:<?= $color?>;'><?=$department['type_name']?></div"
                },
                    "<?=$department['parent_name']?>", ''],
                <?php endforeach;?>
            ]);

            var chart = new google.visualization.OrgChart(document.getElementById('org-chart'));
            chart.draw(data, {'allowHtml': true});
        }
    </script>

</head>
<body>
<div id="org-chart"></div>
</body>
</html>