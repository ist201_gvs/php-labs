<?php
function buildTree(Array $data, $parent = 0) {
    $tree = array();
    foreach ($data as $d) {
        if ($d['parent'] == $parent) {
            $children = buildTree($data, $d['id']);
            // set a trivial key
            if (!empty($children)) {
                $d['_children'] = $children;
            }
            $tree[] = $d;
        }
    }
    return $tree;
}

$categories = [
    1 => ['id' => 1, 'parent' => 0, 'name' => "Sport"],
    2 => ['id' => 2, 'parent' => 6, 'name' => "Ragtime"],
    3 => ['id' => 3, 'parent' => 0, 'name' => "Art"],
    4 => ['id' => 4, 'parent' => 1, 'name' => "Summer Sports"],
    5 => ['id' => 5, 'parent' => 8, 'name' => "Ski"],
    6 => ['id' => 6, 'parent' => 9, 'name' => "Jazz"],
    7 => ['id' => 7, 'parent' => 4, 'name' => "Basketball"],
    8 => ['id' => 8, 'parent' => 1, 'name' => "Winter Sports"],
    9 => ['id' => 9, 'parent' => 3, 'name' => "Music"],
    10 => ['id' => 10, 'parent' => 6, 'name' => "Swing"],
    11 => ['id' => 11, 'parent' => 9, 'name' => "Rock"],
    12 => ['id' => 12, 'parent' => 4, 'name' => "Football"],
    13 => ['id' => 13, 'parent' => 9, 'name' => "Blues"],
    14 => ['id' => 14, 'parent' => 3, 'name' => "Theatre"],
    15 => ['id' => 15, 'parent' => 5, 'name' => "Bobsleigh"],
];
$tree = buildTree($categories);

function printTree($tree, $r = 0, $p = null) {
    foreach ($tree as $i => $t) {
        $dash = ($t['parent'] == 0) ? '' : str_repeat('-', $r) .' ';
        printf("\t<option value='%d'>%s%s</option>\n", $t['id'], $dash, $t['name']);

        if (isset($t['_children'])) {
            printTree($t['_children'], ++$r, $t['parent']);
            --$r;
        }
    }
}

print("<ul>");
printTree($tree);
print("</ul>");