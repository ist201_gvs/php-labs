<?php


namespace app\controllers;

use app\models\Department;
use yii\web\Controller;


class StructureController extends Controller
{
    public function actionTree()
    {
        return $this->render('tree');
    }

    public function actionOrgChart()
    {
        $departments = Department::find()
            ->select('ch.*, pa.department_name as `parent_name`, dt.name as `type_name`')
            ->from('department ch')
            ->leftJoin('department pa', 'ch.parent_id = pa.department_id')
            ->leftJoin('department_type dt', 'ch.type_id = dt.type_id')
            ->asArray()
            ->all();
        return $this->render('org-chart', ['departments' => $departments]);
    }
}